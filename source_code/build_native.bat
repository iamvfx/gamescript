cl /std:c++14 /EHsc /Zi /LD gamescript_library_procedures.cpp

cl /O2 /std:c++14 /EHsc /Zi /DGAME_SCRIPT_NATIVE main.cpp gamescript_imgui_custom_input_text_multiline.cpp ..\source_code_for_redgpu_version\ape.c src\r-lyeh\stdpack.c\stdpack.c ..\source_code_for_redgpu_version\droid_sans_mono.c /I..\source_code_for_redgpu_version /Iinclude /Iinclude\embree /Iinclude\embree\renderer /link /out:game_script_native.exe /implib:game_script_native.implib

cl /std:c++14 /EHsc /Zi /LD /DGAME_SCRIPT_NATIVE /DGS_API="extern \"C\" __declspec(dllexport)" gamescript.cpp /I..\source_code_for_redgpu_version /Iinclude /Iinclude\embree /Iinclude\embree\renderer /link game_script_native.implib

ren gamescript.lib gamescript.dll.lib
