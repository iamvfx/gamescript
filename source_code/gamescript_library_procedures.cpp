#if 0
rm -f libgamescript_library_procedures.so
c++ -std=c++14 -shared -fPIC gamescript_library_procedures.cpp -o libgamescript_library_procedures.so
exit
#endif

// cl /EHsc /std:c++14 /LD gamescript_library_procedures.cpp

#if defined(__linux__)
#include <dlfcn.h>
#elif defined(_WIN32)
#include <Windows.h>
#else
#error Error
#endif

#if defined(__linux__)
#define GS_EXPORT extern "C"
#elif defined(_WIN32)
#define GS_EXPORT extern "C" __declspec(dllexport)
#else
#error Error
#endif

GS_EXPORT void * gsLoadLibrary(const char * filepath) {
#if defined(__linux__)
  return dlopen(filepath, RTLD_LAZY);
#elif defined(_WIN32)
  return (void *)LoadLibraryA(filepath);
#else
#error Error
#endif
}

GS_EXPORT const char * gsLoadLibraryError(const char * filepath) {
#if defined(__linux__)
  return dlerror();
#elif defined(_WIN32)
  return 0;
#else
#error Error
#endif
}

GS_EXPORT void * gsGetProcAddress(void * libraryHandle, const char * procedure) {
#if defined(__linux__)
  return dlsym(libraryHandle, procedure);
#elif defined(_WIN32)
  return (void *)GetProcAddress((HMODULE)libraryHandle, procedure);
#else
#error Error
#endif
}

GS_EXPORT const char * gsGetProcAddressError(void * libraryHandle, const char * procedure) {
#if defined(__linux__)
  return 0;
#elif defined(_WIN32)
  return 0;
#else
#error Error
#endif
}

GS_EXPORT void gsFreeLibrary(void * libraryHandle) {
#if defined(__linux__)
  dlclose(libraryHandle);
#elif defined(_WIN32)
  FreeLibrary((HMODULE)libraryHandle);
#else
#error Error
#endif
}