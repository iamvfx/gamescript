set -x
rm -f libgamescript_library_procedures.so
rm -f libgamescript.so
rm -f game_script_native
cc -c -fPIC -g -O2 -DGAME_SCRIPT_NATIVE ../source_code_for_redgpu_version/ape.c
cc -c -fPIC -g -O2 -DGAME_SCRIPT_NATIVE src/r-lyeh/stdpack.c/stdpack.c
cc -c -fPIC -O2 -DGAME_SCRIPT_NATIVE ../source_code_for_redgpu_version/droid_sans_mono.c
cc -c -fPIC -g -O2 -DGAME_SCRIPT_NATIVE math_finite.c
c++ -shared -fPIC -std=c++14 -g -O2 gamescript_library_procedures.cpp -o libgamescript_library_procedures.so
c++ -shared -fPIC -std=c++14 -g -O2 -DGAME_SCRIPT_NATIVE -DGS_API="extern \"C\"" gamescript.cpp ape.o stdpack.o droid_sans_mono.o -I../source_code_for_redgpu_version -Iinclude -Iinclude/embree -Iinclude/embree/renderer -pthread -lm -ldl *.so libcurl.so.4 libicuuc.so.55 libicudata.so.55 libboost_filesystem.so.1.71.0 lib/ubuntu1604-x64/libloaders.a lib/ubuntu1604-x64/librenderer.a lib/ubuntu1604-x64/librtcore.a lib/ubuntu1604-x64/libsys.a lib/ubuntu1604-x64/libimage.a lib/ubuntu1604-x64/liblexers.a -o libgamescript.so
c++ -rdynamic -fPIC -std=c++14 -g -O2 -DGAME_SCRIPT_NATIVE main.cpp gamescript_imgui_custom_input_text_multiline.cpp ape.o stdpack.o droid_sans_mono.o math_finite.o -I../source_code_for_redgpu_version -Iinclude -Iinclude/embree -Iinclude/embree/renderer -pthread -lm -ldl *.so libcurl.so.4 libidn.so.11 liblber-2.4.so.2 libldap_r-2.4.so.2 libgssapi.so.3 libheimntlm.so.0 libkrb5.so.26 libasn1.so.8 libhcrypto.so.4 libroken.so.18 libwind.so.0 libheimbase.so.1 libhx509.so.5 libicuuc.so.55 libicudata.so.55 libboost_filesystem.so.1.71.0 lib/ubuntu1604-x64/libloaders.a lib/ubuntu1604-x64/librenderer.a lib/ubuntu1604-x64/librtcore.a lib/ubuntu1604-x64/libsys.a lib/ubuntu1604-x64/libimage.a lib/ubuntu1604-x64/liblexers.a -o game_script_native

