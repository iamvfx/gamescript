#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void *       gsLoadLibrary         (const char * filepath);
const char * gsLoadLibraryError    (const char * filepath);
void *       gsGetProcAddress      (void * libraryHandle, const char * procedure);
const char * gsGetProcAddressError (void * libraryHandle, const char * procedure);
void         gsFreeLibrary         (void * libraryHandle);

#ifdef __cplusplus
}
#endif